import { Promise } from 'es6-promise';
import 'whatwg-fetch';
import 'es6-object-assign/auto';

window.Promise = Promise;
