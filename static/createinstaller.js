const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller;
const path = require('path');

getInstallerConfig()
.then(createWindowsInstaller)
.catch((error) => {
  console.error(error.message || error);
  process.exit(1)
});

function getInstallerConfig () {
  console.log('creating windows installer');
  const rootPath = path.join('./');
  const rootPathApp = path.join('./build/release/win/');
  const outPath = path.join(rootPathApp, 'installer');

  return Promise.resolve({
    appDirectory: path.join(rootPathApp, 'unpacked/electronTest-win32-ia32/'),
    authors: 'Pominchuk Evgeniy',
    noMsi: true,
    outputDirectory: outPath,
    exe: 'electronTest.exe',
    setupExe: 'ElectronTestInstaller.exe',
    setupIcon: path.join(rootPath, 'build', 'icons', 'icon.ico')
  })
}
