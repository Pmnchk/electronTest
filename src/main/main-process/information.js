const { ipcMain, dialog } = require('electron');

ipcMain.on('open-information-dialog', (event, opt) => {
  const options = Object.assign({
    // type: 'info',
    title: 'Information',
    message: '',
    buttons: ['Yes', 'No']
  }, opt);
  dialog.showMessageBox(options, (response, checkboxChecked) => {
    event.sender.send('information-dialog-selection', response, checkboxChecked);
  });
});
