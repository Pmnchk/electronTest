import Vue from 'vue';
import axios from 'axios';
import Vuex, { mapGetters } from 'vuex';
import VueRouter from 'vue-router';
import { sync } from 'vuex-router-sync';
import store from './store';
import router from 'router';
import RouterContainer from './screens/router-container';

import VueGettextPlugin from './vue-gettext-plugin';

import mdlDirective from './helpers/directives/mdl';
import clickOutsideDirective from './helpers/directives/click-outside';

import 'material-design-lite/material.min';
import './polyfills';
import './style/entry.scss';

// Plugins
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueGettextPlugin);

// Directives
Vue.directive('mdl', mdlDirective);
Vue.directive('click-outside', clickOutsideDirective);

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));

Vue.http = axios;
Vue.prototype.$http = axios;
Vue.config.productionTip = false;

// VuexStore
const vuexStore = new Vuex.Store(store);
sync(vuexStore, router);

// Mixins
Vue.mixin({
  created() {
    this.console = window.console;
  },
  computed: {
    ...mapGetters({

    }),
  },
});

export default new Vue({
  el: '#app',
  store,
  router,
  render: h => h(RouterContainer)
});
