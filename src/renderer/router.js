import VueRouter from 'vue-router';

import accessConnectionSystem from 'screens/access-connection-system';
import transportRouter from 'screens/transport-router';
import streamsRouter from 'screens/streams-router';

const routes = [
  // Access Connection System
  {
    path: '/access-connection-system',
    component: accessConnectionSystem,
    name: 'access-connection-system',
    alias: ['/acs']
  },
  // Transport Router
  {
    path: '/transport-router',
    component: transportRouter,
    name: 'transport-router',
    alias: ['/tr']
  },
  // Streams Router
  {
    path: '/streams-router',
    component: streamsRouter,
    name: 'streams-router',
    alias: ['/sr']
  },
  // Other
  { path: '*', redirect: '/tr' }
];

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes,
});

// router.beforeEach((to, from, next) => {
//   next();
// });

export default router;
