const { ipcMain, Notification } = require('electron');

ipcMain.on('show-notification', () => {
  const notification = {
    title: 'Basic Notification',
    body: 'Short message part'
  };

  const notifa = new Notification(notification);

  notifa.onclick = () => {
    console.log('Notification clicked');
  };
});
