/* eslint-disable */

process.env.BABEL_ENV = 'renderer';

const path = require('path');
const { dependencies } = require('../package.json');
const webpack = require('webpack');


// const BabiliWebpackPlugin = require('babili-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');

const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

let whiteListedModules = ['vue'];

const { NODE_ENV } = process.env;

const DEVELOPMENT = NODE_ENV === "development";
const PRODUCTION = NODE_ENV === "production";
const PRODUCTION_WATCH = NODE_ENV === "productionWatch";
const PRODUCTION_DEBUG = NODE_ENV === "productionDebug";

const DEBUG_FRIENDLY = DEVELOPMENT || PRODUCTION_DEBUG || PRODUCTION_WATCH;

const useSourceMaps = DEBUG_FRIENDLY;

const publicPath = DEVELOPMENT ? "/" : "/";
// const outputPath = PRODUCTION_DEBUG ? path.resolve(__dirname, './dist/debug') : path.resolve(__dirname, './dist');
const outputPath = path.join(__dirname, '../dist/electron');

const postfix = process.env.postfix ? `-${process.env.postfix}` : '';

let rendererConfig = {
  entry: {
    renderer: path.join(__dirname, '../src/renderer/main.js')
  },
  output: {
    // filename: '[name].js',
    filename: `bundle${postfix}.js`,
    // libraryTarget: 'commonjs2',
    path: outputPath,
    chunkFilename: `[name].bundle${postfix}.js`,
    publicPath
  },
  watch: DEVELOPMENT || PRODUCTION_WATCH,
  devtool: useSourceMaps ? '#cheap-module-eval-source-map' : false,

  devServer: DEVELOPMENT ? {
    host: '0.0.0.0',
    port: 9000,
    historyApiFallback: true,
    hot: true,
    proxy: {
      "/api": {
        target: process.env.PROXY_TARGET || console.log('\x1b[32m', 'Укажи ip-адрес сервера: npm run start -- --env.PROXY_TARGET=https://<ip-адрес>', '\x1b[0m'),
        secure: false
      },
      "/avatar": {
        target: process.env.PROXY_TARGET,
        secure: false
      },
      "/oauth2": {
        target: process.env.PROXY_TARGET,
        secure: false
      },
      "/c/": {
        target: process.env.PROXY_TARGET,
        secure: false
      },
      "/public": {
        target: process.env.PROXY_TARGET,
        secure: false
      },
    },
    // Console output
    stats: {
      children: false,
      entrypoints: false
    }
  } : undefined,
  optimization: {
    minimize: PRODUCTION,
    minimizer: [
      new UglifyJsPlugin({
        cache: false,
      })
    ],
    runtimeChunk: false,
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "initial",
          priority: -10,
          reuseExistingChunk: true,
          enforce: true
        },
      }
    },
  },
  // Console output
  performance : {
    hints : false
  },
  stats: {
    children: false,
    entrypoints: false
  },
  // Loaders
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        loader: "eslint-loader",
        include: /src/,
        exclude: /node_modules/,
        enforce: "pre",
        options: {
          formatter: require("eslint-friendly-formatter"),
          configFile: path.resolve(__dirname, '../.eslintrc.js'),
          ignorePath: path.resolve(__dirname, '../.eslintignore'),
          // fix: true,
          cache: false,
        }
      },
      {
        test: /.vue$/,
        include: /src/,
        loader: 'vue-loader',
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['babel-preset-env'],
            plugins: ['syntax-dynamic-import']
          }
        },
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'resolve-url-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: (loader) => [
                require('autoprefixer')()
              ]
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: (loader) => [
                require('autoprefixer')()
              ]
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif|tif?f)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: '10000',
              name: 'images/[hash].[ext]',
            }
          },
          'img-loader'
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          limit: '10000',
          name: 'fonts/[name].[ext]',
        }
      },
    ],
  },
  externals: [
    ...Object.keys(dependencies || {}).filter(d => !whiteListedModules.includes(d))
  ],
  resolve: {
    extensions: ['*', '.vue', '.js', '.json'],
    alias: {
      '@': path.join(__dirname, '../src/renderer'),
      'vue$': 'vue/dist/vue.esm.js',
      components: path.resolve(__dirname, '../src/renderer/components'),
      helpers: path.resolve(__dirname, '../src/renderer/helpers'),
      assets: path.resolve(__dirname, '../src/renderer/assets'),
      screens: path.resolve(__dirname, '../src/renderer/screens'),
      store: path.resolve(__dirname, '../src/renderer/store'),
      router: path.resolve(__dirname, '../src/renderer/router'),
      //
      'js-modules': process.env.JS_MODULES_PATH ? `${process.env.JS_MODULES_PATH}/dist` : path.resolve(__dirname, './node_modules/trueconf-js-modules/dist'),
      //
      global: path.resolve(__dirname, '../src/renderer/style/_partials.scss'),
      localization: path.resolve(__dirname, '../src/renderer/localization.js'),
      locale: path.resolve(__dirname, '../src/renderer/locale')
    }
  },

  resolveLoader: {
    alias: {
      'svg-inline': 'raw-loader',
    },
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '../src/index.ejs'),
      minify: {
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true
      },
      nodeModules: false
    }),
    new MiniCssExtractPlugin({
      filename: `styles${postfix}.css`,
      chunkFilename: `[name].styles${postfix}.css`
    }),
    new webpack.DefinePlugin({
      DEVELOPMENT,
      PRODUCTION,
    }),
    new webpack.ProvidePlugin({
      // _: 'lodash',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  node: {
    __dirname: !PRODUCTION,
    __filename: !PRODUCTION
  },
  target: 'electron-renderer'
};

/**
 * Adjust rendererConfig for development settings
 */
if (DEVELOPMENT) {
  const developmentPlugins = [
    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),
    // prints more readable module names in the browser console on HMR updates
    new webpack.DefinePlugin({
      '__static': `"${path.join(__dirname, '../static').replace(/\\/g, '\\\\')}"`
    })
  ];

  rendererConfig.plugins.push(...developmentPlugins);
}

// if (process.env.NODE_ENV !== 'production') {
//   rendererConfig.plugins.push(
//     new webpack.DefinePlugin({
//       '__static': `"${path.join(__dirname, '../static').replace(/\\/g, '\\\\')}"`
//     })
//   )
// }

/**
 * Adjust rendererConfig for production settings
 */
if (PRODUCTION) {
  rendererConfig.devtool = '';
  const productionPlugins = [
    new OptimizeCssAssetsPlugin({
      cssProcessorOptions: {
        discardUnused: { fontFace: false },
        discardComments: { removeAll: true },
      },
    }),
    new FaviconsWebpackPlugin({
      // Your source logo
      logo: './src/renderer/assets/main-icon.png',
      // The prefix for all image files (might be a folder or a name)
      prefix: 'icons/',
      // Emit all stats of the generated icons
      emitStats: false,
      // The name of the json containing all favicon information
      statsFilename: 'iconstats-[hash].json',
      // Generate a cache file with control hashes and
      // don't rebuild the favicons until those hashes change
      persistentCache: true,
      // Inject the html into the html-webpack-plugin
      inject: true,
      // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
      background: '#fff',

      // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
      icons: {
        android: true,
        appleIcon: true,
        appleStartup: true,
        coast: false,
        favicons: true,
        firefox: true,
        opengraph: false,
        twitter: false,
        yandex: false,
        windows: false
      }
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"'
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ];

  rendererConfig.plugins.push(...productionPlugins);
}
// if (process.env.NODE_ENV === 'production') {
//   rendererConfig.devtool = '';
//
//   rendererConfig.plugins.push(
//     new BabiliWebpackPlugin(),
//     new CopyWebpackPlugin([
//       {
//         from: path.join(__dirname, '../static'),
//         to: path.join(__dirname, '../dist/electron/static'),
//         ignore: ['.*']
//       }
//     ]),
//     new webpack.DefinePlugin({
//       'process.env.NODE_ENV': '"production"'
//     }),
//     new webpack.LoaderOptionsPlugin({
//       minimize: true
//     })
//   )
// }

module.exports = rendererConfig;
