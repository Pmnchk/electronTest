const fs = require('fs');
const { exec } = require('child_process');
const po2json = require('po2json');

const { GettextExtractor, JsExtractors } = require('gettext-extractor');

const LOCALES_LIST = ['ru_RU', 'pt_PT', 'es_ES', 'pl_PL', 'fr_FR', 'de_DE', 'it_IT', 'be_BY', 'fa_IR', 'zh_CN', 'uk_UA', 'vi_VN'];
const LOCALE_PATH = 'src/renderer/locale/po/';
const JSON_LOCALE_PATH = 'src/renderer/locale/json/';

const MSGMERGE_CMD = 'msgmerge "%poPath" "%potPath" -F -o "%poPath"';
const MSGINIT_CMD = 'msginit -i "%potPath" -o "%output" --locale="%locale" --no-translator';

try {
  fs.accessSync(JSON_LOCALE_PATH);
} catch (e) {
  fs.mkdirSync(JSON_LOCALE_PATH);
}

try {
  fs.accessSync(LOCALE_PATH);
} catch (e) {
  fs.mkdirSync(LOCALE_PATH);
}

function executeCommand(cmd, onExecute) {
  console.log('EXECUTE:', cmd);
  exec(cmd, { encoding: 'buffer' }, (err) => {
    if (err) {
      console.error(err);
    } else {
      onExecute ? onExecute() : null;
    }
  });
}

function createPot() {
  const extractor = new GettextExtractor();

  extractor.createJsParser([
    JsExtractors.callExpression(['$gettext', 'this.$gettext', 'localization.$gettext'], {
      arguments: {
        text: 0
      }
    }),
    JsExtractors.callExpression(['$ngettext', 'this.$ngettext', 'localization.$ngettext'], {
      arguments: {
        text: 0,
        textPlural: 1,
        value: 2
      }
    }),
    JsExtractors.callExpression(['$pgettext', 'this.$pgettext', 'localization.$pgettext'], {
      arguments: {
        context: 0,
        text: 1
      }
    }),
    JsExtractors.callExpression(['$npgettext', 'this.$npgettext', 'localization.$npgettext'], {
      arguments: {
        context: 0,
        text: 1,
        textPlural: 2,
        value: 3
      }
    }),
  ])
  .parseFilesGlob('src/renderer/**/*.@(js|vue)');

  extractor.savePotFile(`${LOCALE_PATH}en_US.pot`);
  console.log('POT-FILE IS SAVED:', `"${LOCALE_PATH}en_US.pot` + `"`);
}

function makeJSON(locale, type) {
  console.log('EXECUTE:', `po2json.parseFile("${LOCALE_PATH}${locale}.${type}")`);

  po2json.parseFile(`${LOCALE_PATH + locale}.${type}`, {
    format: 'jed1.x',
    domain: 'messages',
    stringify: true
  }, (err, jsonData) => {
    fs.open(`${JSON_LOCALE_PATH}/${locale}.json`, 'w', undefined, (err, fd) => {
      fs.writeSync(fd, jsonData);
      fs.close(fd, (err) => {
        if (err) {
          console.error(err);
        }
      });
    });
  });
}

function createPo() {
  for (const locale of LOCALES_LIST) {
    let cmd = '';

    try {
      fs.accessSync(`${LOCALE_PATH + locale}.po`);

      const regexp = /%poPath|%potPath/g;
      const replacements = {
        '%poPath': `${LOCALE_PATH + locale}.po`,
        '%potPath': `${LOCALE_PATH}en_US.pot`
      };

      cmd = MSGMERGE_CMD.replace(regexp, (argName) => {
        return replacements[argName];
      });
    } catch (e) {
      const regexp = /%output|%locale|%potPath/g;
      const replacements = {
        '%output': `${LOCALE_PATH}/${locale}.po`,
        '%locale': locale,
        '%potPath': `${LOCALE_PATH}en_US.pot`
      };

      cmd = MSGINIT_CMD.replace(regexp, (argName) => {
        return replacements[argName];
      });
    } finally {
      executeCommand(cmd, () => {
        makeJSON(locale, 'po');
      });
    }
  }

  makeJSON('en_US', 'pot');
}

createPot();
createPo();
