import { app, BrowserWindow } from 'electron' // eslint-disable-line

const path = require('path');
const glob = require('glob');

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\') // eslint-disable-line
}

let mainWindow;
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:9080'
  : `file://${__dirname}/index.html`;

// Require each JS file in the main-process dir
function load() {
  const files = glob.sync(path.join(__dirname, './main-process/*.js'));
  files.forEach((file) => { require(`./main-process/${file.match('.+\\/(.+)\\.js')[1]}.js`); });
}

function createWindow() {
  load();
  /**
   * Initial window options
   */
  const windowOptions = {
    height: 563,
    useContentSize: true,
    width: 1000,
    frame: true,
    backgroundColor: 'white',
    show: false,
    title: app.getName(),
    webPreferences: {
      nodeIntegration: true
    },
    icon: path.join(__dirname, '../../build/icons/256x256.png')
  };

  mainWindow = new BrowserWindow(windowOptions);

  mainWindow.loadURL(winURL);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
}
if (process.mas) app.setName('electronTest');

app.on('ready', () => {
  createWindow();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

app.setAppUserModelId(process.execPath);

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
