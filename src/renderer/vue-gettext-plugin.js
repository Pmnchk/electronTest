import localization from './localization';

const VueGettextPlugin = {};

VueGettextPlugin.install = (Vue) => {
  Vue.prototype.$gettext = value => localization.gettext(value);

  Vue.prototype.$pgettext = (context, value) => localization.pgettext(context, value);

  Vue.prototype.$npgettext = (context, singular_key, plural_key, value) => localization.npgettext(context, singular_key, plural_key, value);

  Vue.prototype.$setLocale = lang => localization.setLocale(lang);
};

export default VueGettextPlugin;
