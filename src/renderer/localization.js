import Vue from 'vue';
import Jed from 'jed';

export default new Vue({
  methods: {
    gettext(value) {
      if (!this.jed) {
        return value;
      }
      return this.jed.gettext(value);
    },
    pgettext(context, value) {
      if (!this.jed) {
        return value;
      }
      return this.jed.pgettext(context, value);
    },
    npgettext(context, singular_key, plural_key, value) {
      if (!this.jed) {
        return plural_key;
      }
      return this.jed.npgettext(context, singular_key, plural_key, value);
    },
    importLocale(rawLang) {
      const importMap = {
        be: () => import(
          /* webpackChunkName: "locale/locale.be" */
          './locale/json/be_BY.json'
          ),
        de: () => import(
          /* webpackChunkName: "locale/locale.de" */
          './locale/json/de_DE.json'
          ),
        en: () => import(
          /* webpackChunkName: "locale/locale.en" */
          './locale/json/en_US.json'
          ),
        es: () => import(
          /* webpackChunkName: "locale/locale.es" */
          './locale/json/es_ES.json'
          ),
        fa: () => import(
          /* webpackChunkName: "locale/locale.fa" */
          './locale/json/fa_IR.json'
          ),
        fr: () => import(
          /* webpackChunkName: "locale/locale.fr" */
          './locale/json/fr_FR.json'
          ),
        it: () => import(
          /* webpackChunkName: "locale/locale.it" */
          './locale/json/it_IT.json'
          ),
        pl: () => import(
          /* webpackChunkName: "locale/locale.pl" */
          './locale/json/pl_PL.json'
          ),
        pt: () => import(
          /* webpackChunkName: "locale/locale.pt" */
          './locale/json/pt_PT.json'
          ),
        ru: () => import(
          /* webpackChunkName: "locale/locale.ru" */
          './locale/json/ru_RU.json'
          ),
        uk: () => import(
          /* webpackChunkName: "locale/locale.uk" */
          './locale/json/uk_UA.json'
          ),
        vi: () => import(
          /* webpackChunkName: "locale/locale.vi" */
          './locale/json/vi_VN.json'
          ),
        zh: () => import(
          /* webpackChunkName: "locale/locale.zh" */
          './locale/json/zh_CN.json'
          ),
      };

      if (!importMap[rawLang]) {
        return importMap.en();
      }
      return importMap[rawLang]();
    },
    addLang(lang) {
      return this.importLocale(lang)
        .then((json) => {
          const jed = new Jed(json);
          this.jeds[lang] = jed;
        });
    },
    setLocale(lang) {
      if (lang === this.lang) {
        return;
      }
      if (this.jeds[lang]) {
        this.lang = lang;
        return;
      }

      this.addLang(lang)
        .then(() => {
          this.lang = lang;
        });
    }
  },
  computed: {
    jed() {
      return this.jeds[this.lang];
    }
  },
  data: () => ({
    lang: null,
    jeds: {},
  }),
});
